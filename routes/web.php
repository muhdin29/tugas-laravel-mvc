<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



//materi MVC
// Route::get('/test/{angka}', function ($angka) {
    // return view('test', ["angka"=>$angka]);
// });
// Route::get('/halo/{nama}', function ($nama) {
    // return "halo $nama";
// });
// Route::get('/form','RegisterController@form');

// Route::get('/sapa','RegisterController@sapa');

// Route::post('/sapa','RegisterController@sapa_post');



// Tugas laravel
// Route::get('/','HomeController@home');

// Route::get('/register','AuthController@register');

// Route::post('/kirim','AuthController@kirim');

//Route::post('/kirim','AuthController@welcome');

// Route::get('/welcome','AuthController@welcome');



// Materi Blade
// Route::get('/master', function(){
//     return view('adminlte.master');
// });

// route::get('/items',function(){
//     return view('items.index');
// });

// route::get('/create',function(){
//     return view('items.create');
// });



// Tugas Blade
Route::get('/', function(){
        return view('tugasblade.showtable');
    });

Route::get('/data-tables',function(){
    return view('tugasblade.datatables');
    });
